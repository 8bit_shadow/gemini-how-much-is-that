# gemini how much is that

Gitlab repo for 'gemini how much is that?' tampermonkey script.
Adds cost approximated read-outs to the gemini active trader, also makes the bid/ask bars easier to see but the graph has to be smaller to fit all the new info.

<p>
<details>
<summary>Updates:</summary>
<details>
<summary>1.11:</summary>

- {+Added preparations for quick info - in the middle section of the order book.+}
- {+Added dynamic fraction decimal system.+}
- {+Re-enabled commas for price. Quantity and cumulative quantity still crash the site.+}
- {+Fixed cumulative quantity price ($CQ) desync issue. Disabling fade when updating info planned for fast-paced crypto like BTC.+}
- {+Cleanup, comments & some refactoring (more refactoring planned).+}
</details>
</details>
</p>

To install, simply go [here](https://gitlab.com/8bit_shadow/gemini-how-much-is-that/-/raw/main/GHMIT.user.js). Once open, tampermonkey should automatically ask if you want to install the script.
