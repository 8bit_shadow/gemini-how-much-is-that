// ==UserScript==
// @name         Gemini how much is that?
// @namespace    http://tampermonkey.net/
// @version      1.11
// @description  Adds cost approximated read-outs to the gemini active trader, also makes the bid/ask bars easier to see but the graph has to be smaller to fit all the new info.
// @author       You
// @include      /(^https:\/\/exchange\.gemini\.com\/trade\/).*/
// @downloadURL  https://gitlab.com/8bit_shadow/gemini-how-much-is-that/-/raw/main/GHMIT.user.js
// @updateURL    https://gitlab.com/8bit_shadow/gemini-how-much-is-that/-/raw/main/GHMIT.user.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gemini.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let disableFadeOnChange = true;

    // Your code here...
    let forceTwoUpdate = false,
        forceThreeUpdate = false,
        previousCountTwo = [-1, -1, -1, -1],
        //previousCountThree = [-1, -1, -1, -1],
        runOnceUnlessFail = true,
        GSP = -1,//Global Sell Price
        GBP = -1,//Global Buy Price
        GSCQ = -1,//Global Sell Cumulitive Quantity
        GBCQ = -1,//Global Buy Cumulitive Quantity
        GSDCQ = -1,//Global Sell Dollar Cumulitive Quantity
        GBDCQ = -1;//Global Buy Dollar Cumulitive Quantity

    function sleep (time) {//from https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep#39914235
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    //wait 300ms (for main content to load), then run looper.
    sleep(300).then(() => {
        looper();
    });

    function looper()
    {//run the function to loop, then indefinetly loop after 100ms (every 100ms)
        run();//run
        sleep(100).then(() => {
            looper();
        });
    }

    function run()
    {
        if(runOnceUnlessFail)
        {
            runOnce();//run the 'run once' script, until SUCCess
        }

        let thead = document.getElementsByTagName("thead");//get all theads
        if(thead.length >= 3)
        {//if active trader has finished loading

            //-----------------------------------------------------order book
            thead = document.getElementsByTagName("thead")[1];//appending 2nd [1] $ (the order book)
            if(thead.children.length > 0 && (thead.children[0].children.length >= 1 && thead.children[0].children.length < 4))
            {//if the order books' label section loaded
                let clone = thead.children[0].children[0].cloneNode(true);//create a clone of the 'price' element
                clone.children[0].innerHTML = "$Q";//change text
                thead.children[0].prepend(clone);//add clone to the front of the label section

                clone = thead.children[0].children[0].cloneNode(true);//create a clone of the 'price' element
                clone.children[0].innerHTML = "$CQ";//change text
                thead.children[0].prepend(clone);//add clone to the front of the label section

            }

            let head = thead.parentElement.parentElement.children[1];//now looking at entire order book
            if(head.children.length >= 1)
            {//if orders loaded
                if(head.children[0].children[0].children[0].children.length >= 1)
                {//if right SVG loaded
                    if(head.children[0].children[0].children[0].children[0].style.fill != "rgba(202, 58, 58, 0.9)")
                    {//and the SVG has not already been colour corrected
                        head.children[0].children[0].children[0].children[0].style.fill = "rgba(202, 58, 58, 0.9)";//colour correct the SVG by making it less transparent
                    }
                }

                if(head.children[2].children.length >= 1)
                {
                    if(head.children[2].children[0].children.length >= 1)
                    {
                        if(head.children[2].children[0].children[0].style.fill != "rgba(14, 133, 53, 0.9)")
                        {
                            head.children[2].children[0].children[0].style.fill = "rgba(14, 133, 53, 0.9)"
                        }
                    }
                }

                //-------------middle of book
                let midClone = thead;
                if(head.getElementsByTagName("table").length == 2)
                {
                    midClone = thead.parentElement.cloneNode(true);
                    let mid = head.getElementsByClassName("money-value")[0].parentElement;
                    mid.textContent = "";
                    mid.prepend(midClone);
                    midClone.children[0].children[0].children[0].children[0].style.color = "rgba(202, 58, 58, 1)";//recolour $CQ
                    midClone.children[0].children[0].children[1].children[0].style.color = "rgba(14, 133, 53, 1)";//recolour $Q
                    midClone.children[0].children[0].children[3].children[0].style.color = "rgba(14, 133, 53, 1)";//recolour quantity
                    midClone.children[0].children[0].children[4].children[0].style.color = "rgba(202, 58, 58, 1)";//recolour Cumul-quantity

                    midClone.children[0].children[0].children[2].children[0].textContent = "";//price text

                    midClone.children[0].children[0].children[0].children[0].textContent = "";//text $CQ
                    midClone.children[0].children[0].children[1].children[0].textContent = "";//text $Q
                    midClone.children[0].children[0].children[3].children[0].textContent = "";//text quantity
                    midClone.children[0].children[0].children[4].children[0].textContent = "";//text Cumul-quantity
                }
                //-------------

                //-------------sell order book
                let entry = head.children[0];//entry = entire sell order book
                if(entry.children.length >= 1)
                {//if sell order book loaded
                    if(entry.children[0].children.length >= 1)
                    {//if sell order book right chart AND tabel loaded
                        if(entry.children[0].children[1].children.length >= 1)
                        {// if sell order book tabel content loaded
                            thead = entry.children[0].children[1].children[0];//thead = sell order book table body (tbody)
                            if(thead.children.length >= 1)
                            {//if tabel has content
                                //------------------check for updates on the first and second sell order (technically last and 2nd last) to see if we need to force an update
                                let QNTTMP = "";//quantity TMP
                                let theadTMP = thead.children[49];//theadTMP = first sell order (tr)
                                let targ = theadTMP.children[1];//target is 2nd data (td)
                                if(theadTMP.children.length > 4) { targ = theadTMP.children[3]; }//if $CQ and $Q have been added target is instead 4th data (td)

                                QNTTMP = targ.children[0].children[0].textContent;//get the qantity from the text
                                let QNTNTMP = parseFloat(QNTTMP);//parse the number into quntity number TMP (NOT THE SAME AS QNTTMP)
                                if(QNTNTMP != previousCountTwo[0] && previousCountTwo[0] != -1) { forceTwoUpdate = true; }//if a change in price has been detected, force an update to the entire book
                                previousCountTwo[0] = QNTNTMP;//update previous counter for next run

                                theadTMP = thead.children[48];//theadTMP = second sell order (tr)
                                targ = theadTMP.children[1];//target is 2nd data (td)
                                if(theadTMP.children.length > 4) { targ = theadTMP.children[3]; }//if $CQ and $Q have been added target is instead 4th data (td)

                                QNTTMP = targ.children[0].children[0].textContent;//get the qantity from the text
                                QNTNTMP = parseFloat(QNTTMP);//parse the number into quntity number TMP (NOT THE SAME AS QNTTMP)
                                if(QNTNTMP != previousCountTwo[1] && previousCountTwo[1] != -1) { forceTwoUpdate = true; }//if a change in price has been detected, force an update to the entire book
                                previousCountTwo[1] = QNTNTMP;//update previous counter for next run
                                //------------------

                                let priorLoop = thead;//temp store for what thead var was before loop to reset thead after each loop
                                for(let i=thead.children.length - 1; i > -1; i--)
                                {//foreach data (td)
                                    thead = thead.children[i];//thead = ith exchange (tr #i)
                                    if(thead.children.length == 3 || forceTwoUpdate)
                                    {//if current data (td) doesn't have $CQ and $Q or needs to update
                                        let targ = 1;//target = 2nd data (td)
                                        if(thead.children.length > 4) {targ = 3;}//if $CQ and $Q have been added; target = 4th data (td)

                                        let price = "";//price is literal coin value
                                        if(thead.children[targ-1].children[0].children[0].children.length == 1)
                                        {//if text "ask" doesn't exists in quantity
                                            price = thead.children[targ-1].children[0].children[0].textContent;//get the qantity from the text (parsed later)
                                        } else {
                                            price = thead.children[targ-1].children[0].children[0].children[1].textContent.replaceAll("Ask", "");//get the qantity from the text after removing the "ask" text (parsed later)
                                        }

                                        let bidCopy = thead;//a copy element to move the bid colours down to $CQ
                                        if(thead.children[targ-1].children.length >= 1)
                                        {//if the price data (td) has its data loaded (has a span)
                                            if(thead.children[targ-1].children[0].children.length >= 1)
                                            {//and the span has an order book element
                                                if(thead.children[targ-1].children[0].children[0].children.length >= 1)
                                                {//and the order book elements' data is loaded
                                                    let bidE = thead.children[targ-1].children[0].children[0].children[0];//get data, assuming its the bid element
                                                    if(bidE.getAttributeNames().length >= 1)
                                                    {//if the element has at least one attribute (redundentcy for stability)
                                                        if(bidE.getAttributeNames()[0] == 'type')
                                                        {//and the attribute is 'type'
                                                            if(bidE.getAttribute('type') == 'ask')
                                                            {//and the type is 'ask'
                                                                bidE.style.background = "rgba(202, 58, 58, 0.55)";//colour correct the original bid's colour to be more visible
                                                                bidCopy = bidE.cloneNode(true);//create a clone (prepended later)
                                                                bidCopy.style.width = parseFloat(bidE.style.width)*2 + "%";//double the clones width (MUST BE DONE TO COPY, TO DO ONCE)
                                                                bidCopy.style.visibility = "";//ensure the copies visibility is unset (REDUNDANCY REQUIRED FOR FORCE UPDATE)
                                                                bidE.style.visibility = "hidden";//hide the original version
                                                                //bidE.remove();
                                                            }
                                                            else
                                                            {
                                                                console.warn("Gemini how much is that? Attribute error; bid element was detected but its attribute type match failed. A default copy has been made to rescue the session, things might look a bit odd.");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(bidCopy == thead)
                                        {
                                            console.error("Gemini how much is that? Unrecoverable error detected; bid copy failed to enter copy code. 'bidCopy' is still set to thread. Aborting.");
                                            return;
                                        }

                                        let quantity = thead.children[targ].children[0].children[0].textContent;//get the quantity
                                        let CumulitiveQuantity = thead.children[targ+1].children[0].children[0].textContent;//get the cumulitive quantitiy

                                        //---------------$Q and $CQ decimal controller
                                        let MFDQ = 2;//Minimum Fraction Digits Quantity
                                        let MFDCQ = 2;//Minimum Fraction Digits Cumulitive Quantity
                                        let maxFDC = 7;//max Fraction Digits Count

                                        let cost = parseFloat(price.replaceAll(",", "")) * parseFloat(quantity.replaceAll(",", ""));//calculate cost ($Q)
                                        if(parseFloat(cost.toLocaleString('en-US', {minimumFractionDigits: maxFDC})) == 0)
                                        {//if even when at max fraction the cost is still 0
                                            MFDQ = 0;//just show $0
                                        } else {
                                            for(MFDQ=2; MFDQ < maxFDC; MFDQ++)
                                            {//loop maxFDC times, starting from at least 2 fractions
                                                if(parseFloat(cost.toLocaleString('en-US', {minimumFractionDigits: MFDQ})) > 0)
                                                {//if the current fraction no longer shows $0
                                                    break;
                                                }
                                            }
                                        }
                                        cost = cost.toLocaleString('en-US', {minimumFractionDigits: MFDQ});//set cost to updated fraction

                                        let cumulCost = parseFloat(price.replaceAll(",", "")) * parseFloat(CumulitiveQuantity.replaceAll(",", ""));//calculate cost ($CQ)
                                        if(parseFloat(cumulCost.toLocaleString('en-US', {minimumFractionDigits: maxFDC})) == 0)
                                        {//if even when at max fraction the cumul-cost is still 0
                                            MFDCQ = 0;//just show $0
                                        } else {
                                            for(MFDCQ=2; MFDCQ < maxFDC; MFDCQ++)
                                            {//loop maxFDC times, starting from at least 2 fractions
                                                if(parseFloat(cumulCost.toLocaleString('en-US', {minimumFractionDigits: MFDCQ})) > 0)
                                                {//if the current fraction no longer shows $0
                                                    break;
                                                }
                                            }
                                        }
                                        cumulCost = cumulCost.toLocaleString('en-US', {minimumFractionDigits: MFDCQ});//set cumul-cost to updated fraction
                                        //---------------

                                        if(targ == 3)
                                        {//if there's already $Q and $CQ
                                            thead.children[0].remove();//remove them so they can be update (forces fade animation otherwise
                                            thead.children[0].remove();//not stricktly needed, could instead just change the cost directly)
                                        }

                                        let clone = thead.children[0].cloneNode(true);//now looking at first exchanges' timestamp table (td) and duplicating it
                                        clone.children[0].children[0].innerHTML = "$" + cost;//changing the time HTML to $
                                        thead.prepend(clone);//prepending it to the first exchange

                                        clone = thead.children[0].cloneNode(true);//now looking at first exchanges' timestamp table (td) and duplicating it
                                        clone.children[0].children[0].innerHTML = "$" + cumulCost;//changing the time HTML to $
                                        thead.prepend(clone);//prepending it to the first exchange

                                        thead.children[0].children[0].children[0].prepend(bidCopy);//re-add the bid bar but on the (new) left most side

                                        //if any of the prices are 1000 or more, add commas
                                        if(price >= 1000)
                                        {
                                            let t = parseInt(price).toLocaleString('en-US', {minimumFractionDigits: 0});//parse as float then add the commas (.toLocaleString only works on numbers)
                                            let h = parseInt(price);
                                            let tar = thead.children[targ-1].children[0].children[0].outerHTML.replace(h, t);//replace the price with the comma seperated one
                                            thead.children[targ-1].children[0].children[0].outerHTML = tar;//update
                                        }

                                        if(quantity >= 1000)
                                        {
                                            // let t = parseFloat(quantity.replaceAll(",", "")).toLocaleString('en-US', {minimumFractionDigits: 4});//parse as float then add the commas (.toLocaleString only works on numbers)
                                            // thead.children[targ].children[0].children[0].textContent = t;//update
                                        }

                                        if(CumulitiveQuantity >= 1000)
                                        {
                                            // let t = parseFloat(CumulitiveQuantity.replaceAll(",", "")).toLocaleString('en-US', {minimumFractionDigits: 4});//parse as float then add the commas (.toLocaleString only works on numbers)
                                            // thead.children[targ+1].children[0].children[0].children[0].textContent = t;//update
                                        }

                                    }
                                    thead = priorLoop;//resetting thead, now looking at exchanges table body (tbody)
                                }
                                forceTwoUpdate = false;//end force update
                            }
                        }
                    }
                }
                //-------------

                //-------------new buy order book
                entry = head.children[2];//entry = entire sell order book
                if(entry.children.length >= 2)
                {//if order book loaded (looking at svg and tabel)
                    if(entry.children[1].children.length >= 1)
                    {//if order book right chart AND tabel loaded (looking at tbody)
                        if(entry.children[1].children[0].children.length >= 1)
                        {//if order book tabel content loaded (looking at the 50 tr)
                            thead = entry.children[1].children[0];//thead = order book table body (tbody)
                            if(thead.children.length >= 1)
                            {//if tabel has content
                                //------------------check for updates on the first and second sell order (technically last and 2nd last) to see if we need to force an update
                                let QNTTMP = "";//quantity TMP
                                let theadTMP = thead.children[0];//theadTMP = first sell order (tr)
                                let targ = theadTMP.children[1];//target is 2nd data (td)
                                if(theadTMP.children.length > 4) { targ = theadTMP.children[3]; }//if $CQ and $Q have been added target is instead 4th data (td)

                                QNTTMP = targ.children[0].children[0].textContent;//get the qantity from the text
                                let QNTNTMP = parseFloat(QNTTMP);//parse the number into quntity number TMP (NOT THE SAME AS QNTTMP)
                                if(QNTNTMP != previousCountTwo[2] && previousCountTwo[2] != -1) { forceTwoUpdate = true; }//if a change in price has been detected, force an update to the entire book
                                previousCountTwo[2] = QNTNTMP;//update previous counter for next run

                                theadTMP = thead.children[1];//theadTMP = second sell order (tr)
                                targ = theadTMP.children[1];//target is 2nd data (td)
                                if(theadTMP.children.length > 4) { targ = theadTMP.children[3]; }//if $CQ and $Q have been added target is instead 4th data (td)

                                QNTTMP = targ.children[0].children[0].textContent;//get the qantity from the text
                                QNTNTMP = parseFloat(QNTTMP);//parse the number into quntity number TMP (NOT THE SAME AS QNTTMP)
                                if(QNTNTMP != previousCountTwo[3] && previousCountTwo[3] != -1) { forceTwoUpdate = true; }//if a change in price has been detected, force an update to the entire book
                                previousCountTwo[3] = QNTNTMP;//update previous counter for next run
                                //------------------

                                let priorLoop = thead;//temp store for what thead var was before loop to reset thead after each loop
                                for(let i=0; i < thead.children.length; i++)
                                {//foreach data (td)
                                    thead = thead.children[i];//thead = ith exchange (tr #i)
                                    if(thead.children.length == 3 || forceTwoUpdate)
                                    {//if current data (td) doesn't have $CQ and $Q or needs to update
                                        let targ = 1;//target = 2nd data (td)
                                        if(thead.children.length > 4) {targ = 3;}//if $CQ and $Q have been added; target = 4th data (td)

                                        //price is literal coin value
                                        let price = thead.children[targ-1].children[0].children[0].children[1].textContent.replaceAll("Bid", "");//get the qantity from the text after removing the "ask" text (parsed later)

                                        let bidCopy = thead;//a copy element to move the bid colours down to $CQ
                                        if(thead.children[targ-1].children.length >= 1)
                                        {//if the price data (td) has its data loaded (has a span)
                                            if(thead.children[targ-1].children[0].children.length >= 1)
                                            {//and the span has an order book element
                                                if(thead.children[targ-1].children[0].children[0].children.length >= 1)
                                                {//and the order book elements' data is loaded
                                                    let bidE = thead.children[targ-1].children[0].children[0].children[0];//get data, assuming its the bid element
                                                    bidCopy = bidE.cloneNode(true);//create default a clone (prepended later)
                                                    bidCopy.style.visibility = "hidden";//hide it until needed
                                                    if(bidE.getAttributeNames().length >= 1)
                                                    {//if the element has at least one attribute (redundentcy for stability)
                                                        if(bidE.getAttributeNames()[0] == 'type')
                                                        {//and the attribute is 'type'
                                                            if(bidE.getAttribute('type') == 'bid')
                                                            {//and the type is 'ask'
                                                                bidCopy.style.background = "rgba(14, 133, 53, 0.55)";//colour correct the original bid's colour to be more visible
                                                                bidCopy.style.width = parseFloat(bidE.style.width)*2 + "%";//double the clones width (MUST BE DONE TO COPY, TO DO ONCE)
                                                                bidCopy.style.visibility = "";//ensure the copies visibility is unset (REDUNDANCY REQUIRED FOR FORCE UPDATE)
                                                                bidE.style.visibility = "hidden";//hide the original version
                                                                //bidE.remove();
                                                            }
                                                            else
                                                            {
                                                                console.warn("Gemini how much is that? Attribute error; bid element was detected but its attribute type match failed. A default copy has been made to rescue the session, things might look a bit odd.");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(bidCopy == thead)
                                        {
                                            console.error("Gemini how much is that? Unrecoverable error detected; bid copy failed to enter copy code. 'bidCopy' is still set to thread. Aborting.");
                                            return;
                                        }

                                        let quantity = thead.children[targ].children[0].children[0].textContent;//get the quantity
                                        let CumulitiveQuantity = thead.children[targ+1].children[0].children[0].textContent;//get the cumulitive quantitiy

                                        //---------------$Q and $CQ decimal controller
                                        let MFDQ = 2;//Minimum Fraction Digits Quantity
                                        let MFDCQ = 2;//Minimum Fraction Digits Cumulitive Quantity
                                        let maxFDC = 7;//max Fraction Digits Count

                                        let cost = parseFloat(price.replaceAll(",", "")) * parseFloat(quantity.replaceAll(",", ""));//calculate cost ($Q)
                                        if(parseFloat(cost.toLocaleString('en-US', {minimumFractionDigits: maxFDC})) == 0)
                                        {//if even when at max fraction the cost is still 0
                                            MFDQ = 0;//just show $0
                                        } else {
                                            for(MFDQ=2; MFDQ < maxFDC; MFDQ++)
                                            {//loop maxFDC times, starting from at least 2 fractions
                                                if(parseFloat(cost.toLocaleString('en-US', {minimumFractionDigits: MFDQ})) > 0)
                                                {//if the current fraction no longer shows $0
                                                    break;
                                                }
                                            }
                                        }
                                        cost = cost.toLocaleString('en-US', {minimumFractionDigits: MFDQ});//set cost to updated fraction

                                        let cumulCost = parseFloat(price.replaceAll(",", "")) * parseFloat(CumulitiveQuantity.replaceAll(",", ""));//calculate cost ($CQ)
                                        if(parseFloat(cumulCost.toLocaleString('en-US', {minimumFractionDigits: maxFDC})) == 0)
                                        {//if even when at max fraction the cumul-cost is still 0
                                            MFDCQ = 0;//just show $0
                                        } else {
                                            for(MFDCQ=2; MFDCQ < maxFDC; MFDCQ++)
                                            {//loop maxFDC times, starting from at least 2 fractions
                                                if(parseFloat(cumulCost.toLocaleString('en-US', {minimumFractionDigits: MFDCQ})) > 0)
                                                {//if the current fraction no longer shows $0
                                                    break;
                                                }
                                            }
                                        }
                                        cumulCost = cumulCost.toLocaleString('en-US', {minimumFractionDigits: MFDCQ});//set cumul-cost to updated fraction
                                        //---------------

                                        if(targ == 3)
                                        {//if there's already $Q and $CQ
                                            thead.children[0].remove();//remove them so they can be update (forces fade animation otherwise
                                            thead.children[0].remove();//not stricktly needed, could instead just change the cost directly)
                                        }

                                        let clone = thead.children[0].cloneNode(true);//now looking price (td) and duplicating it
                                        clone.children[0].children[0].innerHTML = "$" + cost;//changing the time HTML to $
                                        thead.prepend(clone);//prepending it to the first exchange

                                        clone = thead.children[0].cloneNode(true);//now looking at price (td) and duplicating it
                                        clone.children[0].children[0].innerHTML = "$" + cumulCost;//changing the time HTML to $
                                        thead.prepend(clone);//prepending it to the first exchange

                                        //thead.children[0].children[0].children[0].prepend(bidCopy);//re-add the bid bar but on the (new) left most side

                                        //if any of the prices are 1000 or more, add commas
                                        if(price >= 1000)
                                        {
                                            let t = parseInt(price).toLocaleString('en-US', {minimumFractionDigits: 0});//parse as float then add the commas (.toLocaleString only works on numbers)
                                            let h = parseInt(price);
                                            let tar = thead.children[targ-1].children[0].children[0].outerHTML.replace(h, t);//replace the price with the comma seperated one
                                            thead.children[targ-1].children[0].children[0].outerHTML = tar;//update
                                        }

                                        if(quantity >= 1000)
                                        {
                                            // let t = parseFloat(quantity.replaceAll(",", "")).toLocaleString('en-US', {minimumFractionDigits: 4});//parse as float then add the commas (.toLocaleString only works on numbers)
                                            // thead.children[targ].children[0].children[0].textContent = t;//update
                                        }

                                        if(CumulitiveQuantity >= 1000)
                                        {
                                            // let t = parseFloat(CumulitiveQuantity.replaceAll(",", "")).toLocaleString('en-US', {minimumFractionDigits: 4});//parse as float then add the commas (.toLocaleString only works on numbers)
                                            // thead.children[targ+1].children[0].children[0].children[0].textContent = t;//update
                                        }

                                    }
                                    thead = priorLoop;//resetting thead, now looking at exchanges table body (tbody)
                                }
                                forceTwoUpdate = false;//end force update
                            }
                        }
                    }
                }//-------------
            }//-----------------------------------------------------

            thead = document.getElementsByTagName("thead")[3];//appending 3rd [2] $ (the order history)
            if(thead.children.length > 0)
            {
                if(thead.children[0].children.length >= 1 && thead.children[0].children.length < 4)
                {
                    let clone = thead.children[0].children[0].cloneNode(true);
                    clone.children[0].innerHTML = "$";
                    thead.children[0].prepend(clone);
                }
            }


            head = thead.parentElement.parentElement;//now looking at entire section
            if(head.children.length >= 2)
            {
                thead = head.children[1];//now looking at exchanges div
                if(thead.children.length >= 1)
                {
                    thead = thead.children[0];//now looking at exchanges table
                    if(thead.children.length >= 1)
                    {
                        thead = thead.children[0];//now looking at exchanges table body (tbody)
                        if(thead.children.length >= 1)
                        {
                            let amountToCheck = thead.children.length;
                            let priorLoop = thead;
                            for(let i=0; i< amountToCheck; i++)
                            {//foreach loop through every item
                                thead = thead.children[i];//now looking at first exchange (tr)
                                if(thead.children.length >= 3 && thead.children.length < 4)
                                {
                                    let price = thead.children[1].children[0].children[0].innerHTML;
                                    let quantity = thead.children[2].children[0].children[0].innerHTML;
                                    let cost = parseFloat(price) * parseFloat(quantity);
                                    cost = cost.toLocaleString('en-US', {minimumFractionDigits: 2});
                                    let clone = thead.children[0].cloneNode(true);//now looking at first exchanges' timestamp table (td) and duplicating it
                                    clone.children[0].children[0].innerHTML = "$" + cost;//changing the time HTML to $
                                    thead.prepend(clone);//prepending it to the first exchange

                                    if(price > 10)
                                    {
                                        thead.children[2].children[0].children[0].textContent = parseFloat(price).toLocaleString('en-US', {minimumFractionDigits: 4});
                                    }

                                    if(quantity > 10)
                                    {
                                        thead.children[3].children[0].children[0].textContent = parseFloat(quantity).toLocaleString('en-US', {minimumFractionDigits: 4});
                                    }

                                }
                                thead = priorLoop;//resetting thead, now looking at exchanges table body (tbody)
                            }

                        }

                    }

                }
            }
        }

    }

    function runOnce()
    {
        if(document.body.children.length >= 1)
        {
            if(document.body.children[0].children.length >= 1)
            {
                if(document.body.children[0].children[0].children.length >= 3)
                {
                    document.body.children[0].children[0].children[2].style.gridTemplateColumns = "239px 2fr 1.05fr 2fr"
                    runOnceUnlessFail = false;
                }
            }
        }


    }

})();
